﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Server.Extensions;
using Server.Frames;

namespace Server.PayloadEnumerators
{
    internal class StatefulServerFrameEnumerator : IEnumerator<ITypedServerFrame>
    {
        private readonly int seedValue;
        private readonly ushort seedSequenceNumber;
        private readonly ushort sequenceLength;

        private uint currentValue;
        private ushort currentSequenceNumber;

        private Random random;
        private SHA256Managed hash;
        private bool isComplete;

        public StatefulServerFrameEnumerator(int seedValue, ushort sequenceNumber, ushort sequenceLength)
        {
            this.seedValue = seedValue;
            this.seedSequenceNumber = sequenceNumber;
            this.sequenceLength = sequenceLength;

            this.hash = new SHA256Managed();
            this.InitializeRandom();
        }

        public ITypedServerFrame Current
        {
            get
            {
                if (this.isComplete)
                {
                    return new TypedStatefulServerCloseFrame(this.currentSequenceNumber, this.currentValue, this.hash.Hash);
                }
                else
                {
                    return new TypedStatefulServerFrame(ServerMessageType.Data, this.currentSequenceNumber, this.currentValue);
                }
            }
        }

        object IEnumerator.Current => this.Current;

        public void Dispose()
        {
            this.random = null;
        }

        public bool MoveNext()
        {
            if (this.currentSequenceNumber == this.sequenceLength)
            {
                return false;
            }

            this.currentValue = this.GetRandomUint32();

            if (this.currentSequenceNumber == 0)
            {
                this.currentSequenceNumber = this.seedSequenceNumber;
            }
            else
            {
                this.currentSequenceNumber++;
            }


            if (this.currentSequenceNumber == this.sequenceLength)
            {
                this.hash.TransformFinalBlock(new byte[0], 0, 0);
                this.isComplete = true;
            }

            return true;
        }

        private void AddToHash(uint value)
        {
            byte[] data = value.ToByteArray();
            this.hash.TransformBlock(data, 0, sizeof(uint), data, 0);
        }

        public void Reset()
        {
            this.currentValue = 0;
            this.currentSequenceNumber = 0;

            this.hash = new SHA256Managed();
            this.InitializeRandom();
        }
        
        private void InitializeRandom()
        {
            this.random = new Random(this.seedValue);
            // Advance the PRNG to the seed sequence number
            for (int i = 0; i < this.seedSequenceNumber - 1; i++)
            {
                this.GetRandomUint32();
            }
        }

        private uint GetRandomUint32()
        {
            uint result = (uint)this.random.Next(int.MinValue, int.MaxValue);

            this.AddToHash(result);

            return result;
        }
    }
}
