﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;

using Server.Frames;

namespace Server.PayloadEnumerators
{
    internal class StatelessServerFrameEnumerator : IEnumerator<ITypedServerFrame>
    {
        private readonly BigInteger seedValue;
        private readonly ushort seedSequenceNumber;

        public StatelessServerFrameEnumerator(BigInteger seedValue, ushort sequenceNumber)
        {
            this.seedValue = seedValue;
            this.seedSequenceNumber = sequenceNumber;
        }

        public ITypedServerFrame Current => new TypedStatelessServerFrame(ServerMessageType.Data, this.CurrentSequenceNumber, this.CurrentValue);
        object IEnumerator.Current => this.Current;

        public BigInteger CurrentValue { get; private set; }
        public ushort CurrentSequenceNumber { get; private set; }

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            if (this.CurrentSequenceNumber == 0)
            {
                this.CurrentValue = this.seedValue;
                this.CurrentSequenceNumber = this.seedSequenceNumber;
            }
            else
            {
                this.CurrentValue *= 2;
                this.CurrentSequenceNumber++;
            }

            return true;
        }

        public void Reset()
        {
            this.CurrentValue = 0;
            this.CurrentSequenceNumber = 0;
        }
    }
}
