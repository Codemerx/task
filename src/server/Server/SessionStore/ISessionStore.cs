﻿namespace Server.SessionStore
{
    public interface ISessionStore
    {
        void AddClientData(string id, ClientData value);

        bool TryGetClientData(string id, out ClientData value);

        void Remove(string id);

        void SetExpiration(string id, long milliseconds);

        void CancelExpiration(string id);
    }
}
