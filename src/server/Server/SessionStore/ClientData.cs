﻿namespace Server.SessionStore
{
    public class ClientData
    {
        public ClientData(int prngSeed, ushort numberOfMessagesToSend)
        {
            this.PrngSeed = prngSeed;
            this.NumberOfMessagesToSend = numberOfMessagesToSend;
        }

        public int PrngSeed { get; private set; }

        public ushort NumberOfMessagesToSend { get; private set; }
    }
}
