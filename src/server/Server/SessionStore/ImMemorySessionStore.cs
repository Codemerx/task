﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Server.SessionStore
{
    internal class ImMemorySessionStore : ISessionStore
    {
        private ConcurrentDictionary<string, ClientData> clients;
        private ConcurrentDictionary<string, ExpirationData> expiringClients;

        public ImMemorySessionStore()
        {
            this.clients = new ConcurrentDictionary<string, ClientData>();
            this.expiringClients = new ConcurrentDictionary<string, ExpirationData>();
        }

        public void AddClientData(string id, ClientData value)
        {
            if (!this.clients.TryAdd(id, value))
            {
                throw new ArgumentException("This key already exists.");
            }
        }

        public bool TryGetClientData(string id, out ClientData value)
        {
            return this.clients.TryGetValue(id, out value);
        }

        public void Remove(string id)
        {
            this.clients.Remove(id, out ClientData clientData);
        }

        public void SetExpiration(string id, long milliseconds)
        {
            this.expiringClients.TryAdd(id, new ExpirationData(DateTime.Now.AddMilliseconds(milliseconds)));

            this.RunExpirationManager();
        }

        public void CancelExpiration(string id)
        {
            if (this.expiringClients.TryGetValue(id, out ExpirationData data))
            {
                data.Cancel();
            }
        }

        private void RunExpirationManager()
        {
            Task.Run(async () =>
            {
                while (this.expiringClients.Count > 0)
                {
                    await Task.Delay(1000);

                    List<string> clientsToRemove = new List<string>();
                    List<string> expirationsToRemove = new List<string>();

                    foreach (KeyValuePair<string, ExpirationData> item in this.expiringClients)
                    {
                        if (item.Value.Token.IsCancellationRequested)
                        {
                            expirationsToRemove.Add(item.Key);

                            continue;
                        }

                        if (DateTime.Now >= item.Value.ExpiryDate)
                        {
                            clientsToRemove.Add(item.Key);
                            expirationsToRemove.Add(item.Key);
                        }
                    }

                    foreach (string clientId in clientsToRemove)
                    {
                        this.clients.Remove(clientId, out ClientData clientData);
                    }

                    foreach (string clientId in expirationsToRemove)
                    {
                        this.expiringClients.Remove(clientId, out ExpirationData value);
                    }
                }
            });
        }

        private class ExpirationData
        {
            private readonly CancellationTokenSource tokenSource;

            public ExpirationData(DateTime expiryDate)
            {
                ExpiryDate = expiryDate;

                this.tokenSource = new CancellationTokenSource();
            }

            public DateTime ExpiryDate { get; }

            public CancellationToken Token
            {
                get
                {
                    return this.tokenSource.Token;
                }
            }

            public void Cancel()
            {
                this.tokenSource.Cancel();
            }
        }
    }
}
