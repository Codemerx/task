﻿using System;

namespace Server
{
    internal static class Logger
    {
        public static bool Enabled { get; internal set; } = true;

        public static void Log(string message)
        {
            if (Enabled)
            {
                Console.WriteLine(message);
            }
        }

        public static void ForceLog(string message)
        {
            Console.WriteLine(message);
        }
    }
}
