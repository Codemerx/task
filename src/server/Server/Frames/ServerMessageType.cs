﻿namespace Server.Frames
{
    internal enum ServerMessageType : byte
    {
        Data = 0,
        Close = 1
    }
}