﻿using System;

using Grpc.Core;

using Server.Extensions;

namespace Server.Frames
{
    internal class TypedClientFrame
    {
        public TypedClientFrame(ClientFrame clientFrame)
        {
            byte connectionType = clientFrame.ConnectionType.AsByte();
            if (!Enum.IsDefined(typeof(ConnectionType), connectionType))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, $"Unsupported connection type - {connectionType}"));
            }

            byte messageType = clientFrame.MessageType.AsByte();
            if (!Enum.IsDefined(typeof(ClientMessageType), messageType))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, $"Unsupported message type - {messageType}"));
            }

            this.ConnectionType = (ConnectionType)connectionType;
            this.MessageType = (ClientMessageType)messageType;
            this.PayloadSize = clientFrame.PayloadSize.AsByte();
            this.Payload = clientFrame.Payload.ToByteArray();
        }

        protected TypedClientFrame(TypedClientFrame clientFrame)
        {
            this.ConnectionType = clientFrame.ConnectionType;
            this.MessageType = clientFrame.MessageType;
            this.PayloadSize = clientFrame.PayloadSize;
            this.Payload = clientFrame.Payload;
        }

        public ConnectionType ConnectionType { get; }

        public ClientMessageType MessageType { get; }

        public ushort PayloadSize { get; }

        public byte[] Payload { get; }
    }
}
