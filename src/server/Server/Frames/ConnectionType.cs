﻿namespace Server.Frames
{
    internal enum ConnectionType : byte
    {
        Stateless = 0,
        Stateful = 1
    }
}
