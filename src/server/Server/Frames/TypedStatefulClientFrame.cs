﻿using System;

using Server.Extensions;

namespace Server.Frames
{
    internal class TypedStatefulClientFrame : TypedClientFrame
    {
        public TypedStatefulClientFrame(TypedClientFrame clientFrame)
            : base(clientFrame)
        {
            int sizeOfUuid = 36;

            ReadOnlySpan<byte> uuidSpan = new ReadOnlySpan<byte>(this.Payload, 0, sizeOfUuid);
            this.Uuid = uuidSpan.ToActualString();

            ReadOnlySpan<byte> numberOfMessagesToSendSpan = new ReadOnlySpan<byte>(this.Payload, sizeOfUuid, sizeof(ushort));
            this.NumberOfMessagesToSend = numberOfMessagesToSendSpan.ToUShort();
        }

        public string Uuid { get; }

        public ushort NumberOfMessagesToSend { get; }
    }
}
