﻿namespace Server.Frames
{
    internal interface ITypedServerFrame
    {
        ServerFrame ToServerFrame();
    }
}
