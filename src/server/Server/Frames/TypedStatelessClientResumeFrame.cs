﻿using Server.Extensions;
using System;
using System.Numerics;

namespace Server.Frames
{
    internal class TypedStatelessClientResumeFrame : TypedClientResumeFrame
    {
        public TypedStatelessClientResumeFrame(TypedClientFrame clientFrame)
            : base(clientFrame)
        {
            int sizeOfLastSequenceNumber = sizeof(ushort);

            ReadOnlySpan<byte> lastValueSpan = new ReadOnlySpan<byte>(this.Payload, sizeOfLastSequenceNumber, this.PayloadSize - sizeOfLastSequenceNumber);
            string lastValueAsString = lastValueSpan.ToActualString();
            this.LastValue = BigInteger.Parse(lastValueAsString);
        }

        public BigInteger LastValue { get; }
    }
}
