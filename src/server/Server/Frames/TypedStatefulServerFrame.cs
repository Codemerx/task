﻿using Google.Protobuf;

using Server.Extensions;

namespace Server.Frames
{
    internal class TypedStatefulServerFrame : TypedServerFrameBase, ITypedServerFrame
    {
        private uint currentValue;

        public TypedStatefulServerFrame(ServerMessageType messageType, ushort currentSequenceNumber, uint currentValue)
            : base(messageType, currentSequenceNumber)
        {
            this.currentValue = currentValue;
        }

        protected override ByteString GetPayload()
        {
            ByteString payload = this.currentValue.ToByteString();
            return payload;
        }
    }
}