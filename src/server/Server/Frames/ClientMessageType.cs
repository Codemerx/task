﻿namespace Server.Frames
{
    internal enum ClientMessageType : byte
    {
        Start = 0,
        Resume = 1,
        Close = 2
    }
}
