﻿using Google.Protobuf;

namespace Server.Frames
{
    internal class TypedStatefulServerCloseFrame : TypedStatefulServerFrame, ITypedServerFrame
    {
        private byte[] hash;

        public TypedStatefulServerCloseFrame(ushort currentSequenceNumber, uint currentValue, byte[] hash)
            : base(ServerMessageType.Close, currentSequenceNumber, currentValue)
        {
            this.hash = hash;
        }

        protected override ByteString GetPayload()
        {
            ByteString basePayload = base.GetPayload();

            byte[] payload = new byte[basePayload.Length + this.hash.Length];
            basePayload.CopyTo(payload, 0);
            this.hash.CopyTo(payload, basePayload.Length);

            return ByteString.CopyFrom(payload);
        }
    }
}