﻿using System.Numerics;

using Google.Protobuf;

using Server.Extensions;

namespace Server.Frames
{
    internal class TypedStatelessServerFrame : TypedServerFrameBase, ITypedServerFrame
    {
        private BigInteger currentValue;

        public TypedStatelessServerFrame(ServerMessageType serverMessageType, ushort messageSequenceNumber, BigInteger currentValue)
            : base(serverMessageType, messageSequenceNumber)
        {
            this.currentValue = currentValue;
        }

        protected override ByteString GetPayload()
        {
            ByteString payload = currentValue.ToString().ToByteString();
            return payload;
        }
    }
}
