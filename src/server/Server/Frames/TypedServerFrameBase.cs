﻿using Google.Protobuf;

using Server.Extensions;

namespace Server.Frames
{
    internal abstract class TypedServerFrameBase : ITypedServerFrame
    {
        private ServerMessageType messageType;
        private ushort messageSequenceNumber;

        public TypedServerFrameBase(ServerMessageType messageType, ushort messageSequenceNumber)
        {
            this.messageType = messageType;
            this.messageSequenceNumber = messageSequenceNumber;
        }

        public ServerFrame ToServerFrame()
        {
            ByteString payload = this.GetPayload();

            return new ServerFrame()
            {
                MessageType = ((byte)this.messageType).ToByteString(),
                MessageSequenceNumber = this.messageSequenceNumber.ToByteString(),
                PayloadSize = ((ushort)payload.Length).ToByteString(),
                Payload = payload
            };
        }

        protected abstract ByteString GetPayload();
    }
}
