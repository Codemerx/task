﻿using Server.Extensions;
using System;

namespace Server.Frames
{
    internal class TypedStatefulClientResumeFrame : TypedClientResumeFrame
    {
        public TypedStatefulClientResumeFrame(TypedClientFrame clientFrame)
            : base(clientFrame)
        {
            ReadOnlySpan<byte> uuidSpan = new ReadOnlySpan<byte>(this.Payload, sizeof(ushort), 36);
            this.Uuid = uuidSpan.ToActualString();
        }

        public string Uuid { get; }
    }
}
