﻿using System;

using Server.Extensions;

namespace Server.Frames
{
    internal class TypedClientResumeFrame : TypedClientFrame
    {
        public TypedClientResumeFrame(TypedClientFrame clientFrame)
            : base(clientFrame)
        {
            int sizeOfLastSequenceNumber = sizeof(ushort);

            ReadOnlySpan<byte> lastSequenceNumberSpan = new ReadOnlySpan<byte>(this.Payload, 0, sizeOfLastSequenceNumber);
            this.LastSequenceNumber = lastSequenceNumberSpan.ToUShort();
        }

        protected TypedClientResumeFrame(TypedClientResumeFrame clientFrame)
            : base(clientFrame)
        {
            this.LastSequenceNumber = clientFrame.LastSequenceNumber;
        }

        public ushort LastSequenceNumber { get; }
    }
}
