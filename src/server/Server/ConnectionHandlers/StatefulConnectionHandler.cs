﻿using System.Threading.Tasks;

using Grpc.Core;

using Server.Frames;
using Server.PayloadEnumerators;
using Server.SessionStore;

namespace Server.ConnectionHandlers
{
    internal class StatefulConnectionHandler : ConnectionHandlerBase, IConnectionHandler
    {
        private ISessionStore store;
        private Task<bool> payloadGeneratorTask;
        private string uuid;

        public StatefulConnectionHandler(ISessionStore store)
        {
            this.store = store;
        }

        public override ConnectionType ConnectionType => ConnectionType.Stateful;

        protected override Task HandleConnect(TypedClientFrame clientFrame, IServerStreamWriter<ServerFrame> responseStream, ServerCallContext context)
        {
            TypedStatefulClientFrame statefulClientFrame = new TypedStatefulClientFrame(clientFrame);

            this.CheckForExistingConnection(statefulClientFrame.Uuid);

            int prngSeed = randomGenerator.Next();
            Logger.Log($"New client connected in stateful mode:\n\tUUID: {statefulClientFrame.Uuid}\n\tNumber of values to send: {statefulClientFrame.NumberOfMessagesToSend}\n\tPRNG seed: {prngSeed}");

            this.uuid = statefulClientFrame.Uuid;
            
            ClientData clientData = new ClientData(prngSeed, statefulClientFrame.NumberOfMessagesToSend);
            this.store.AddClientData(statefulClientFrame.Uuid, clientData);

            if (this.IsTestMode)
            {
                Logger.ForceLog(clientData.PrngSeed.ToString());
            }
            
            StatefulServerFrameEnumerator enumerator = new StatefulServerFrameEnumerator(clientData.PrngSeed, 1, clientData.NumberOfMessagesToSend);

            this.payloadGeneratorTask = RunPayloadGenerator(enumerator, responseStream, this.payloadGeneratorCancellation.Token);

            return Task.CompletedTask;
        }

        protected override Task HandleResume(TypedClientFrame clientFrame, IServerStreamWriter<ServerFrame> responseStream, ServerCallContext context)
        {
            TypedStatefulClientResumeFrame resumeFrame = new TypedStatefulClientResumeFrame(clientFrame);

            this.store.CancelExpiration(resumeFrame.Uuid);

            if (!this.store.TryGetClientData(resumeFrame.Uuid, out ClientData clientData))
            {
                Logger.Log($"Client {resumeFrame.Uuid} tried to reconnect after the 30s time frame available for reconnects.");

                throw new RpcException(new Status(StatusCode.FailedPrecondition, "Failed to reconnect during the allowed time frame of 30s. Closing connection..."));
            }

            Logger.Log($"Client reconnected in stateful mode:\n\tUUID: {resumeFrame.Uuid}\n\tLast received value is #{resumeFrame.LastSequenceNumber}");

            this.uuid = resumeFrame.Uuid;

            StatefulServerFrameEnumerator enumerator = new StatefulServerFrameEnumerator(clientData.PrngSeed, resumeFrame.LastSequenceNumber, clientData.NumberOfMessagesToSend);
            enumerator.MoveNext();  // Move the enumerator to the next (i.e. first unreceived by the client) value

            this.payloadGeneratorTask = RunPayloadGenerator(enumerator, responseStream, this.payloadGeneratorCancellation.Token);

            return Task.CompletedTask;
        }

        protected override Task HandleClose(TypedClientFrame clientFrame)
        {
            throw new RpcException(new Status(StatusCode.InvalidArgument, "Stateful connections does not support client close requests."));
        }

        private void CheckForExistingConnection(string uuid)
        {
            if (this.store.TryGetClientData(uuid, out ClientData clientData))
            {
                Logger.Log($"Client with UUID {uuid} tried to connect again. Rejecting connection...");

                throw new RpcException(new Status(StatusCode.FailedPrecondition, "Client with the same UUID is already connected."));
            }
        }

        internal override async Task HandleOnClosing()
        {
            bool completedSuccessfully = await this.payloadGeneratorTask;
            if (completedSuccessfully)
            {
                this.state = ConnectionState.Closed;

                Logger.Log($"Connection with client {this.uuid} closed sucessfully.");

                this.store.Remove(this.uuid);
            }
            else // Connection is dropped
            {
                this.state = ConnectionState.Dropped;

                Logger.Log($"Connection with client {this.uuid} dropped.");

                this.store.SetExpiration(this.uuid, 30000);
            }
        }
    }
}
