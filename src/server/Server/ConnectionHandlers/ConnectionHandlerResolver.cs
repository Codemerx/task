﻿using System;

using Server.Frames;
using Server.SessionStore;

namespace Server.ConnectionHandlers
{
    internal class ConnectionHandlerResolver
    {
        private ISessionStore store;

        private ConnectionHandlerResolver()
        {
            this.store = new ImMemorySessionStore();
        }

        public static ConnectionHandlerResolver Instance { get; } = new ConnectionHandlerResolver();

        public IConnectionHandler Resolve(TypedClientFrame clientFrame)
        {
            if (clientFrame.ConnectionType == ConnectionType.Stateless)
            {
                return new StatelessConnectionHandler();
            }
            else if (clientFrame.ConnectionType == ConnectionType.Stateful)
            {
                return new StatefulConnectionHandler(this.store);
            }
            else
            {
                // Throw not implemented exception, so if new connection type is
                // introduced and it is not added here the developer would see it.
                throw new NotImplementedException();
            }
        }
    }
}
