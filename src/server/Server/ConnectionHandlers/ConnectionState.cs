﻿namespace Server.ConnectionHandlers
{
    internal enum ConnectionState
    {
        New,
        Started,
        Resumed,
        Closed,
        Dropped
    }
}
