﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Grpc.Core;

using Server.Frames;

namespace Server.ConnectionHandlers
{
    internal abstract class ConnectionHandlerBase : IConnectionHandler
    {
        protected static Random randomGenerator = new Random();

        protected ConnectionState state;
        protected CancellationTokenSource payloadGeneratorCancellation;

        public ConnectionHandlerBase()
        {
            this.payloadGeneratorCancellation = new CancellationTokenSource();
        }

        public abstract ConnectionType ConnectionType { get; }

        protected bool IsTestMode { get; private set; }
        
        public async Task Handle(IAsyncStreamReader<ClientFrame> requestStream, IServerStreamWriter<ServerFrame> responseStream, ServerCallContext context, bool isTestMode)
        {
            Logger.Log(string.Empty);
            Logger.Log("Incomming connection...");

            this.state = ConnectionState.New;

            this.IsTestMode = isTestMode;

            do
            {
                ClientFrame currentFrame = requestStream.Current;
                TypedClientFrame clientFrame = new TypedClientFrame(currentFrame);

                if (clientFrame.MessageType == ClientMessageType.Start)
                {
                    if (this.state != ConnectionState.New)
                    {
                        throw new RpcException(new Status(StatusCode.FailedPrecondition, "Client start frame can be send only to new connections."));
                    }

                    await this.HandleConnect(clientFrame, responseStream, context);

                    this.state = ConnectionState.Started;
                }
                else if (clientFrame.MessageType == ClientMessageType.Resume)
                {
                    if (this.state != ConnectionState.New)
                    {
                        throw new RpcException(new Status(StatusCode.FailedPrecondition, "Client resume frame can be send only to new connections."));
                    }

                    await this.HandleResume(clientFrame, responseStream, context);

                    this.state = ConnectionState.Resumed;
                }
                else if (clientFrame.MessageType == ClientMessageType.Close)
                {
                    if (this.state != ConnectionState.Started && this.state != ConnectionState.Resumed)
                    {
                        throw new RpcException(new Status(StatusCode.FailedPrecondition, "Client close frame can be send only to already started or resumed connections."));
                    }

                    await this.HandleClose(clientFrame);

                    this.state = ConnectionState.Closed;

                    break;
                }
            } while (await requestStream.MoveNext(CancellationToken.None));

            this.payloadGeneratorCancellation.Cancel();

            await this.HandleOnClosing();
        }

        internal abstract Task HandleOnClosing();

        protected abstract Task HandleConnect(TypedClientFrame clientFrame, IServerStreamWriter<ServerFrame> responseStream, ServerCallContext context);

        protected abstract Task HandleResume(TypedClientFrame clientFrame, IServerStreamWriter<ServerFrame> responseStream, ServerCallContext context);

        protected abstract Task HandleClose(TypedClientFrame clientFrame);

        protected Task<bool> RunPayloadGenerator(IEnumerator<ITypedServerFrame> enumerator, IServerStreamWriter<ServerFrame> responseStream, CancellationToken payloadGeneratorCancellation)
        {
            return Task.Run<bool>(async () =>
            {
                return await PayloadGenerator(enumerator, responseStream, payloadGeneratorCancellation);
            });
        }

        private async Task<bool> PayloadGenerator(IEnumerator<ITypedServerFrame> enumerator, IServerStreamWriter<ServerFrame> responseStream, CancellationToken payloadGeneratorCancellation)
        {
            while (enumerator.MoveNext())
            {
                await Task.Delay(1000);

                if (payloadGeneratorCancellation.IsCancellationRequested)
                {
                    return false;
                }

                ITypedServerFrame typedServerFrame = enumerator.Current;

                await responseStream.WriteAsync(typedServerFrame.ToServerFrame());
            }

            return true;
        }
    }
}
