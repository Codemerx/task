﻿using System.Numerics;
using System.Threading.Tasks;

using Grpc.Core;

using Server.Frames;
using Server.PayloadEnumerators;

namespace Server.ConnectionHandlers
{
    internal class StatelessConnectionHandler : ConnectionHandlerBase, IConnectionHandler
    {
        private Task<bool> payloadGenerator;
        
        public override ConnectionType ConnectionType => ConnectionType.Stateless;

        protected override Task HandleConnect(TypedClientFrame clientFrame, IServerStreamWriter<ServerFrame> responseStream, ServerCallContext context)
        {
            BigInteger a = randomGenerator.Next(1, 0xFF + 1);   // The maxValue is exclusive so + 1 to include 0xFF

            Logger.Log($"New client connected in stateless mode:\n\tSeed generated: {a}.");
            if (this.IsTestMode)
            {
                Logger.ForceLog(a.ToString());
            }

            StatelessServerFrameEnumerator enumerator = new StatelessServerFrameEnumerator(a, 1);
            
            this.payloadGenerator = RunPayloadGenerator(enumerator, responseStream, this.payloadGeneratorCancellation.Token);

            return Task.CompletedTask;
        }

        protected override Task HandleResume(TypedClientFrame clientFrame, IServerStreamWriter<ServerFrame> responseStream, ServerCallContext context)
        {
            TypedStatelessClientResumeFrame resumeFrame = new TypedStatelessClientResumeFrame(clientFrame);

            Logger.Log($"Client reconnected in stateless mode:\n\tLast received value: #{resumeFrame.LastSequenceNumber} - {resumeFrame.LastValue}");

            StatelessServerFrameEnumerator enumerator = new StatelessServerFrameEnumerator(resumeFrame.LastValue, resumeFrame.LastSequenceNumber);
            enumerator.MoveNext();  // Move the enumerator to the next (i.e. first unreceived by the client) value

            this.payloadGenerator = RunPayloadGenerator(enumerator, responseStream, this.payloadGeneratorCancellation.Token);

            return Task.CompletedTask;
        }

        protected override async Task HandleClose(TypedClientFrame clientFrame)
        {
            Logger.Log("Client close request received. Closing connection...");

            payloadGeneratorCancellation.Cancel();

            await this.payloadGenerator;
        }

        internal override Task HandleOnClosing()
        {
            if (this.state == ConnectionState.Closed)
            {
                Logger.Log("Connection closed.");
            }
            else
            {
                this.state = ConnectionState.Dropped;

                Logger.Log("Connection dropped.");
            }

            return Task.CompletedTask;
        }
    }
}