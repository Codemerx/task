﻿using System.Threading.Tasks;

using Grpc.Core;

namespace Server.ConnectionHandlers
{
    internal interface IConnectionHandler
    {
        Frames.ConnectionType ConnectionType { get; }

        Task Handle(IAsyncStreamReader<ClientFrame> requestStream, IServerStreamWriter<ServerFrame> responseStream, ServerCallContext context, bool testMode);
    }
}
