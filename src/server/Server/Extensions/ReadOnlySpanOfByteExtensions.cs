﻿using System;
using System.Text;

namespace Server.Extensions
{
    internal static class ReadOnlySpanOfByteExtensions
    {
        public static ushort ToUShort(this ReadOnlySpan<byte> span)
        {
            return BitConverter.ToUInt16(span);
        }

        public static string ToActualString(this ReadOnlySpan<byte> span)
        {
            return Encoding.UTF8.GetString(span);
        }
    }
}
