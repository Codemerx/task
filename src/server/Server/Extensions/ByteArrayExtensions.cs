﻿using Google.Protobuf;

namespace Server.Extensions
{
    internal static class ByteArrayExtensions
    {
        public static ByteString ToByteString(this byte[] bytes)
        {
            EndianessHelper.EnsureCorrectEndianess(bytes);

            return ByteString.CopyFrom(bytes);
        }
    }
}
