﻿using Google.Protobuf;

namespace Server.Extensions
{
    internal static class StringExtensions
    {
        public static ByteString ToByteString(this string str)
        {
            return ByteString.CopyFromUtf8(str);
        }
    }
}
