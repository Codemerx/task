﻿using Google.Protobuf;

namespace Server.Extensions
{
    internal static class ByteExtensions
    {
        public static ByteString ToByteString(this byte number)
        {
            return ByteString.CopyFrom(number);
        }
    }
}
