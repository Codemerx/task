﻿using System;

namespace Server.Extensions
{
    internal static class EndianessHelper
    {
        public static void EnsureCorrectEndianess(this byte[] bytes)
        {
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }
        }
    }
}
