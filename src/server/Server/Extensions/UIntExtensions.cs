﻿using System;

using Google.Protobuf;

namespace Server.Extensions
{
    internal static class UIntExtensions
    {
        public static ByteString ToByteString(this uint number)
        {
            return ByteString.CopyFrom(number.ToByteArray());
        }

        public static byte[] ToByteArray(this uint number)
        {
            byte[] byteArray = BitConverter.GetBytes(number);

            EndianessHelper.EnsureCorrectEndianess(byteArray);

            return byteArray;
        }
    }
}
