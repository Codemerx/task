﻿using System;

using Google.Protobuf;

namespace Server.Extensions
{
    internal static class ByteStringExtensions
    {
        public static byte AsByte(this ByteString byteString)
        {
            return byteString[0];
        }

        public static ushort AsUShort(this ByteString byteString)
        {
            byte[] bytes = byteString.ToByteArray();

            EndianessHelper.EnsureCorrectEndianess(bytes);

            return BitConverter.ToUInt16(bytes);
        }

        public static Guid AsGuid(this ByteString byteString)
        {
            return new Guid(byteString.ToStringUtf8());
        }
    }
}
