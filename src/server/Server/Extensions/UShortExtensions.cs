﻿using System;

using Google.Protobuf;

namespace Server.Extensions
{
    internal static class UShortExtensions
    {
        public static ByteString ToByteString(this ushort number)
        {
            byte[] byteArray = BitConverter.GetBytes(number);

            EndianessHelper.EnsureCorrectEndianess(byteArray);

            return ByteString.CopyFrom(byteArray);
        }
    }
}
