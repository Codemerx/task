﻿using System;
using System.Collections.Generic;

using Grpc.Core;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            (int port, bool isTestMode, string parseResult) = ParseArguments(args);

            if (port == 0)
            {
                Logger.Log(parseResult);
                return;
            }

            if (isTestMode)
            {
                Logger.Enabled = false;
            }

            Grpc.Core.Server server = new Grpc.Core.Server(GetServerOptions())
            {
                Services = { StatefulStatelessProtocol.BindService(new Server(isTestMode)) },
                Ports = { new ServerPort("localhost", port, ServerCredentials.Insecure) }
            };

            server.Start();

            Logger.Log($"Server listening on localhost:{port}");
            Logger.Log("Press any key to stop the server...");
            Console.ReadKey();
            Logger.Log("Shutting down the server...");

            server.ShutdownAsync().Wait();
        }

        private static IEnumerable<ChannelOption> GetServerOptions()
        {
            return new List<ChannelOption>()
            {
                new ChannelOption("grpc.keepalive_time_ms", 250),
                new ChannelOption("grpc.keepalive_timeout_ms", 250),
                new ChannelOption("grpc.keepalive_permit_without_calls", 1),
                new ChannelOption("grpc.http2.max_pings_without_data", 0),
                new ChannelOption("grpc.http2.min_time_between_pings_ms", 500),
                new ChannelOption("grpc.http2.min_ping_interval_without_data_ms", 500)
            };
        }

        private static (int, bool, string) ParseArguments(string[] args)
        {
            string usageSample = "\n\nSample usage: \n dotnet run -c Release -- 9595";
            int port = 0;
            bool isTestMode = false;

            if (args.Length < 1)
                return (port, isTestMode, "First argument should be port number" + usageSample);

            if (!Int32.TryParse(args[0], out port) || port < 1024 || port > 65535)
                return (port, isTestMode, "Port number should be a numerical value between 1024 and 65535" + usageSample);

            if (args.Length >= 2 && args[1] == "/test")
            {
                isTestMode = true;
            }

            return (port, isTestMode, "OK");
        }
    }
}
