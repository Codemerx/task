@ECHO OFF

SET ProtoToolsPath=%userprofile%\.nuget\packages\grpc.tools\1.17.0\tools\windows_x64

%ProtoToolsPath%\protoc.exe -I..\..\proto --csharp_out .\proto --grpc_out .\proto --plugin=protoc-gen-grpc=%ProtoToolsPath%\grpc_csharp_plugin.exe StatefulStatelessProtocol.proto