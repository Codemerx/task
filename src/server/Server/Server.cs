﻿using System.Threading;
using System.Threading.Tasks;

using Grpc.Core;

using Server.ConnectionHandlers;
using Server.Frames;

using static StatefulStatelessProtocol;

namespace Server
{
    internal class Server : StatefulStatelessProtocolBase
    {
        private readonly bool isTestMode;

        public Server(bool isTestMode)
        {
            this.isTestMode = isTestMode;
        }

        public override async Task Connect(IAsyncStreamReader<ClientFrame> requestStream, IServerStreamWriter<ServerFrame> responseStream, ServerCallContext context)
        {
            if (await requestStream.MoveNext(CancellationToken.None))
            {
                ClientFrame currentFrame = requestStream.Current;
                TypedClientFrame clientFrame = new TypedClientFrame(currentFrame);

                IConnectionHandler handler = ConnectionHandlerResolver.Instance.Resolve(clientFrame);
                await handler.Handle(requestStream, responseStream, context, this.isTestMode);
            }
        }
    }
}
