﻿using System;
using System.Security.Cryptography;

namespace Tests
{
    internal static class StatefulExpectedResultCalculator
    {
        internal static string Calculate(string line, int numberOfMessages)
        {
            Random random = new Random(int.Parse(line));
            SHA256Managed hash = new SHA256Managed();

            for (int i = 0; i < numberOfMessages; i++)
            {
                uint value = (uint)random.Next(int.MinValue, int.MaxValue);
                byte[] data = value.ToByteArray();
                hash.TransformBlock(data, 0, sizeof(uint), data, 0);
            }

            hash.TransformFinalBlock(new byte[0], 0, 0);

            return BitConverter.ToString(hash.Hash).Replace("-", "").ToLowerInvariant();
        }
        
        public static byte[] ToByteArray(this uint number)
        {
            byte[] byteArray = BitConverter.GetBytes(number);

            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(byteArray);
            }

            return byteArray;
        }
    }
}