﻿using System;
using System.Numerics;

namespace Tests
{
    internal class StatelessExpectedResultCalculator
    {
        internal static string Calculate(string line, int numberOfMessages)
        {
            BigInteger sum = 0;
            BigInteger current = BigInteger.Parse(line);
            sum = current;
            for (int i = 0; i < numberOfMessages - 1; i++)
            {
                current = current * 2;
                sum = sum + current;
            }

            return sum.ToString();
        }
    }
}