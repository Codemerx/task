﻿using System;
using System.IO;

namespace Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            string usageSample = "\n\nUsage:\ndotnet run </stateless|/stateful|/mixed> <path-to-server-dll> <path-to-client-js> [<number-of-clients>]";

            if (args.Length < 3)
            {
                Console.WriteLine("At least 3 arguments expected." + usageSample);
                return;
            }

            string mode = args[0];
            if (mode != "/stateless" && mode != "/stateful" && mode != "/mixed")
            {
                Console.WriteLine("\"/stateless\", \"/stateful\" or \"/mixed\" is expected as first argument." + usageSample);
                return;
            }

            string pathToServer = args[1];
            if (!File.Exists(pathToServer) || Path.GetExtension(pathToServer) != ".dll")
            {
                Console.WriteLine("Path to server executable expected as second argument." + usageSample);
                return;
            }

            string pathToClient = args[2];
            if (!File.Exists(pathToClient) || Path.GetExtension(pathToClient) != ".js")
            {
                Console.WriteLine("Path to client entry point file expected as third argument." + usageSample);
                return;
            }

            int numberOfClientsToLaunch = 1;
            if (args.Length == 4)
            {
                if (!int.TryParse(args[3], out numberOfClientsToLaunch))
                {
                    Console.WriteLine("Number of clients to lauch is expected as fourth argument." + usageSample);
                    return;
                }
            }

            Test test;
            if (mode == "/stateless")
            {
                test = new StatelessTest(pathToServer, pathToClient, numberOfClientsToLaunch);
            }
            else if (mode == "/stateful")
            {
                test = new StatefulTest(pathToServer, pathToClient, numberOfClientsToLaunch);
            }
            else
            {
                test = new MixedTest(pathToServer, pathToClient, numberOfClientsToLaunch);
            }

            test.Run();

            Console.WriteLine("---------------------------");
            if (test.IsSuccessful)
            {
                Console.WriteLine("Test PASSED.");
            }
            else
            {
                Console.WriteLine("Test FAILED.");
            }
        }
    }
}
