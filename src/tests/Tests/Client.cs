﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Tests
{
    internal abstract class Client
    {
        protected string pathToClient;

        public Client(int clientNumber, string pathToClient, int numberOfMessages)
        {
            this.ClientNumber = clientNumber;
            this.pathToClient = pathToClient;
            this.NumberOfMessages = numberOfMessages;
        }

        public abstract ClientMode Mode { get; }

        public int ClientNumber { get; private set; }

        public int NumberOfMessages { get; private set; }

        public string ExpectedResult { get; set; }

        public bool IsSuccessful { get; private set; }

        public Task Start()
        {
            ProcessStartInfo clientStartInfo = new ProcessStartInfo("node", this.GetArguments())
            {
                RedirectStandardOutput = true
            };

            Process clientProcess = Process.Start(clientStartInfo);
            return Task.Run(() =>
            {
                string actualResult = clientProcess.StandardOutput.ReadLine();
                if (actualResult == this.ExpectedResult)
                {
                    this.IsSuccessful = true;

                    Console.WriteLine($"Client #{this.ClientNumber} passed the test.");
                }
                else
                {
                    Console.WriteLine($"Client #{this.ClientNumber} failed the test. Expected: {this.ExpectedResult}. Actual: {actualResult}");
                }

                clientProcess.Close();
            });
        }

        internal abstract string GetArguments();
    }
}
