﻿namespace Tests
{
    internal class StatefulTest : Test
    {
        public StatefulTest(string pathToServer, string pathToClient, int numberOfClientsToLaunch)
            : base(pathToServer, pathToClient, numberOfClientsToLaunch)
        {
        }

        protected override Client StartClient(int clientNumber, string pathToClient, int numberOfMessages)
        {
            return new StatefulClient(clientNumber, pathToClient, numberOfMessages);
        }

        internal override string GetExpectedResult(string line, Client client)
        {
            return StatefulExpectedResultCalculator.Calculate(line, client.NumberOfMessages);
        }
    }
}