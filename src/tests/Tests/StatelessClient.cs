﻿namespace Tests
{
    internal class StatelessClient : Client
    {
        public StatelessClient(int clientNumber, string pathToClient, int numberOfMessages)
            : base(clientNumber, pathToClient, numberOfMessages)
        {
        }

        public override ClientMode Mode => ClientMode.Stateless;

        internal override string GetArguments()
        {
            return $"--no-warnings --no-deprecation {this.pathToClient} /stateless localhost:9595 {this.NumberOfMessages} /test";
        }
    }
}
