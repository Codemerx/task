﻿namespace Tests
{
    internal class StatefulClient : Client
    {
        public StatefulClient(int clientNumber, string pathToClient, int numberOfMessages)
            : base(clientNumber, pathToClient, numberOfMessages)
        {
        }

        public override ClientMode Mode => ClientMode.Stateful;

        internal override string GetArguments()
        {
            return $"--no-warnings --no-deprecation {this.pathToClient} /stateful localhost:9595 /test {this.NumberOfMessages}";
        }
    }
}
