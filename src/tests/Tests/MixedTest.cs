﻿using System;

namespace Tests
{
    internal class MixedTest : Test
    {
        private static Random random = new Random();

        public MixedTest(string pathToServer, string pathToClient, int numberOfClientsToLaunch)
            : base(pathToServer, pathToClient, numberOfClientsToLaunch)
        {
        }

        protected override Client StartClient(int clientNumber, string pathToClient, int numberOfMessages)
        {
            if (random.Next() % 2 == 0)
            {
                return new StatefulClient(clientNumber, pathToClient, numberOfMessages);
            }
            else
            {
                return new StatelessClient(clientNumber, pathToClient, numberOfMessages);
            }
        }

        internal override string GetExpectedResult(string line, Client client)
        {
            if (client.Mode == ClientMode.Stateful)
            {
                return StatefulExpectedResultCalculator.Calculate(line, client.NumberOfMessages);
            }
            else if (client.Mode == ClientMode.Stateless)
            {
                return StatelessExpectedResultCalculator.Calculate(line, client.NumberOfMessages);
            }
            else
            {
                throw new ArgumentException("Unsupported client mode.");
            }
        }
    }
}
