﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Tests
{
    internal abstract class Test
    {
        private static Random random = new Random();

        private BlockingCollection<Client> newClients;
        private ConcurrentBag<Client> clients;
        private ManualResetEvent sync;

        private string pathToServer;
        private string pathToClient;
        private int numberOfClientsToLaunch;

        public Test(string pathToServer, string pathToClient, int numberOfClientsToLaunch)
        {
            this.newClients = new BlockingCollection<Client>();    // Defaults to FIFO
            this.clients = new ConcurrentBag<Client>();
            this.sync = new ManualResetEvent(true);

            this.pathToServer = pathToServer;
            this.pathToClient = pathToClient;
            this.numberOfClientsToLaunch = numberOfClientsToLaunch;
        }

        public bool IsSuccessful { get; private set; }

        internal void Run()
        {
            int port = 9595;
            Console.WriteLine($"Starting server at localhost:{port}");
            ProcessStartInfo serverStartInfo = new ProcessStartInfo("dotnet", $"{this.pathToServer} {port} /test")
            {
                RedirectStandardOutput = true
            };
            Process serverProcess = Process.Start(serverStartInfo);
            Task serverTask = Task.Run(() =>
            {
                while (true)
                {
                    Client client;
                    try
                    {
                         client = this.newClients.Take();
                    }
                    catch (InvalidOperationException)
                    {
                        break;
                    }

                    string line = serverProcess.StandardOutput.ReadLine();
                    Console.WriteLine($"Client #{client.ClientNumber} - SEED = {line}");
                    client.ExpectedResult = this.GetExpectedResult(line, client);
                    this.sync.Set();
                }
            });

            List<Task> clientTasks = new List<Task>();

            for (int i = 0; i < this.numberOfClientsToLaunch; i++)
            {
                this.sync.WaitOne();
                this.sync.Reset();


                int numberOfMessages = random.Next(1, 100);
                Client client = this.StartClient(i + 1, this.pathToClient, numberOfMessages);
                Console.WriteLine($"Starting {client.Mode.ToString().ToLowerInvariant()} client #{i + 1} with N = {numberOfMessages}");
                clientTasks.Add(client.Start());
                this.newClients.Add(client);
                this.clients.Add(client);
            }

            this.newClients.CompleteAdding();

            serverTask.Wait();
            Task.WaitAll(clientTasks.ToArray());

            serverProcess.Kill();
            
            this.IsSuccessful = this.clients.All(client => client.IsSuccessful);
        }

        internal abstract string GetExpectedResult(string line, Client client);
        protected abstract Client StartClient(int clientNumber, string pathToClient, int numberOfMessages);
    }
}