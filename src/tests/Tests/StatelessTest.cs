﻿namespace Tests
{
    internal class StatelessTest : Test
    {
        public StatelessTest(string pathToServer, string pathToClient, int numberOfClientsToLaunch)
            : base(pathToServer, pathToClient, numberOfClientsToLaunch)
        {
        }

        protected override Client StartClient(int clientNumber, string pathToClient, int numberOfMessages)
        {
            return new StatelessClient(clientNumber, pathToClient, numberOfMessages);
        }

        internal override string GetExpectedResult(string line, Client client)
        {
            return StatelessExpectedResultCalculator.Calculate(line, client.NumberOfMessages);
        }
    }
}
