import { BigInteger } from 'big-integer';

export function stringToBuffer(str: string) : Buffer {
    return Buffer.from(str, 'utf8');
}

export function uint8ToUint8Array(num: number) : Uint8Array {
    const buffer = Buffer.alloc(1);
    buffer.writeUInt8(num, 0);
    return buffer;
}

export function uint8ArrayToUint8(array: Uint8Array) : number {
    const buffer = Buffer.from(array);
    return buffer.readUInt8(0);
}

export function uint16ToBuffer(num: number) : Buffer {
    const buffer = Buffer.alloc(2);
    buffer.writeUInt16LE(num, 0);
    return buffer;
}

export function uint8ArrayToUint16(array: Uint8Array) : number {
    const buffer = Buffer.from(array);
    return buffer.readUInt16LE(0);
}

export function uint8ArrayToString(array: Uint8Array) : string {
    const buffer = Buffer.from(array);
    return buffer.toString('utf8');
}

export function bigIntegerToString(bigInteger: BigInteger) : string {
    return bigInteger.toArray(10).value.join('');
}