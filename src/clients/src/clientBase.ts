import { StatusObject, status, credentials, ClientDuplexStream } from "grpc";

import { Logger } from "./logger";
import { ConnectionState } from "./connectionState";
import { ClientFrame, ServerFrame } from "../proto/StatefulStatelessProtocol_pb";
import { StatefulStatelessProtocolClient } from "../proto/StatefulStatelessProtocol_grpc_pb";
import { TypedServerFrame } from "./frames/typedServerFrame";

export abstract class ClientBase {
    private readonly grpcClient: StatefulStatelessProtocolClient;
    
    protected connectionState: ConnectionState = ConnectionState.NotConnected;
    
    constructor(url: string, protected numberOfMessagesToReceive: number, protected isTestMode: boolean) {
        this.grpcClient = new StatefulStatelessProtocolClient(url, credentials.createInsecure(), this.getClientOptions());
    }

    protected abstract getClientStartFrame() : ClientFrame;
    protected abstract getClientResumeFrame() : ClientFrame;

    protected abstract onData(data: ServerFrame, stream: ClientDuplexStream<ClientFrame, ServerFrame>) : void;

    protected abstract getResult() : string;
    
    private connect(callback: (string) => void) : void {
        Logger.log(`Connecting to ${this.grpcClient.getChannel().getTarget()}...`);
        const clientStartFrame = this.getClientStartFrame();
        this.connectInternal(clientStartFrame, callback);
    }

    private reconnect(callback: (string) => void) : void {
        Logger.log('Reconnecting...');
        const clientResumeFrame = this.getClientResumeFrame();
        this.connectInternal(clientResumeFrame, callback);
    }

    public start(callback: (string) => void) : void {
        this.connect(callback);
    }

    protected connectInternal(clientFrame: ClientFrame, callback: (string) => void) : void {
        const stream = this.grpcClient.connect();
        stream.write(clientFrame);
        stream.once('data', () => {
            if (this.connectionState === ConnectionState.NotConnected) {
                Logger.log('Connected.');
            } else if (this.connectionState === ConnectionState.Dropped) {
                Logger.log('Reconnected.');
            }

            this.connectionState = ConnectionState.Connected;
        });
        stream.on('data', data => this.onData(data, stream));
        stream.on('end', () => this.onEnd(callback));
        stream.on('error', (error: StatusObject) => this.onError(error, callback));
    }

    protected onError(error: StatusObject, callback: (string) => void) : void {
        if (error.code === status.UNAVAILABLE) {
            if (this.connectionState === ConnectionState.NotConnected) {
                Logger.log('Connection failed.');

                this.connect(callback);
            } else if (this.connectionState === ConnectionState.Connected) {
                Logger.log('Connection dropped.');

                this.connectionState = ConnectionState.Dropped;
                this.reconnect(callback)
            } else if (this.connectionState === ConnectionState.Dropped) {
                Logger.log('Reconnection failed.');

                // setTimeout-ing the reconnect call fixes some weird behavior where the default backoff algorithm
                // does not kick in and many reconnect calls are made without any wait between them.
                setTimeout(() => this.reconnect(callback));
            }
        } else {
            Logger.log(`Error: ${error.details}`);
        }
    }

    private onEnd(callback: (string) => void) : void {
        Logger.log('Connection closed successfully.');

        this.connectionState = ConnectionState.Closed;
        callback(this.getResult());
    }

    private getClientOptions() : object {
        return {
            "grpc.keepalive_time_ms": 1000,
            "grpc.keepalive_timeout_ms": 1000,
            "grpc.keepalive_permit_without_calls": 1,
            "grpc.http2.min_time_between_pings_ms": 2000,
            "grpc.initial_reconnect_backoff_ms": 100,
            "grpc.min_reconnect_backoff_ms": 200,
            "grpc.max_reconnect_backoff_ms": 5000
        };
    }
}
