export enum ConnectionState {
    NotConnected = 1,
    Connected = 2,
    Dropped = 3,
    Closed = 4
}
