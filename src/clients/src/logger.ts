export class Logger {
    public static enabled: boolean = true;

    public static log(str: string) : void {
        if (this.enabled) {
            console.log(str);
        }
    }

    public static forceLog(str: string) : void {
        console.log(str);
    }
}