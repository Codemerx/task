export enum ClientMessageType {
    Start = 0,
    Resume = 1,
    Close = 2
}
