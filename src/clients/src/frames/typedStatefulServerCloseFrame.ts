import { TypedStatefulServerFrame } from "./typedStatefulServerFrame";
import { ServerFrame } from "../../proto/StatefulStatelessProtocol_pb";

export class TypedStatefulServerCloseFrame extends TypedStatefulServerFrame {
    public hash: Buffer;

    constructor(serverFrame: ServerFrame) {
        super(serverFrame);

        const arr = serverFrame.getPayload_asU8();
        this.hash = Buffer.from(arr.buffer, arr.byteOffset + this.usedBytesFromPayload);
    }
}
