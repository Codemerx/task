import { ClientFrame } from "../../proto/StatefulStatelessProtocol_pb";

import { TypedClientFrame } from "./typedClientFrame";
import { uint16ToBuffer, stringToBuffer, uint8ToUint8Array, bigIntegerToString } from "../conversionHelpers";
import { ClientMessageType } from "./clientMessageType";
import { ConnectionType } from "./connectionType";

export class TypedClientResumeFrame extends TypedClientFrame {
    constructor(connectionType: ConnectionType, private lastSequenceNumber: number) {
        super(connectionType, ClientMessageType.Resume);
    }

    public toClientFrame() : ClientFrame {
        const result = super.toClientFrame();

        const payload = uint16ToBuffer(this.lastSequenceNumber);
        result.setPayloadsize(uint8ToUint8Array(payload.length));
        result.setPayload(payload);

        return result;
    }
}
