export enum ConnectionType {
    Stateless = 0,
    Stateful = 1
}