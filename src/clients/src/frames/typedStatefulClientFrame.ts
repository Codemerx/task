import { ClientFrame } from "../../proto/StatefulStatelessProtocol_pb";

import { TypedClientFrame } from "./typedClientFrame";
import { ClientMessageType } from "./clientMessageType";
import { stringToBuffer, uint16ToBuffer, uint8ToUint8Array } from '../conversionHelpers';
import { ConnectionType } from "./connectionType";

export class TypedStatefulClientFrame extends TypedClientFrame {
    constructor(connectionType: ConnectionType, messageType: ClientMessageType, private uuid: string, private numberOfMessages: number) {
        super(connectionType, messageType);
    }

    public toClientFrame() : ClientFrame {
        const result = super.toClientFrame();

        const uuidAsUint8Array = stringToBuffer(this.uuid);
        const numberOfMessagesAsUint8Array = uint16ToBuffer(this.numberOfMessages);

        const payload = new Buffer(uuidAsUint8Array.length + numberOfMessagesAsUint8Array.length);
        uuidAsUint8Array.copy(payload);
        numberOfMessagesAsUint8Array.copy(payload, uuidAsUint8Array.length);

        result.setPayloadsize(uint8ToUint8Array(payload.length));
        result.setPayload(payload);

        return result;
    }
}
