import bigInt from "big-integer";

import { ServerFrame } from "../../proto/StatefulStatelessProtocol_pb";

import { TypedServerFrame } from "./typedServerFrame";
import { uint8ArrayToString } from "../conversionHelpers";

export class TypedStatelessServerFrame extends TypedServerFrame {
    public currentValue: bigInt.BigInteger;

    constructor(serverFrame: ServerFrame) {
        super(serverFrame);

        this.currentValue = bigInt(uint8ArrayToString(serverFrame.getPayload_asU8()));
    }
}
