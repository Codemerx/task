import { ServerFrame } from '../../proto/StatefulStatelessProtocol_pb';

import { uint8ArrayToUint16, uint8ArrayToUint8 } from '../conversionHelpers';
import { ServerMessageType } from './serverMessageType';

export class TypedServerFrame {
    public messageType: ServerMessageType;
    public messageSequenceNumber: number;

    constructor(serverFrame: ServerFrame) {
        this.messageType = uint8ArrayToUint8(serverFrame.getMessagetype_asU8());
        this.messageSequenceNumber = uint8ArrayToUint16(serverFrame.getMessagesequencenumber_asU8());
    }
}
