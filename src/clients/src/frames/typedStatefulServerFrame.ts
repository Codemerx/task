import { ServerFrame } from "../../proto/StatefulStatelessProtocol_pb";

import { TypedServerFrame } from "./typedServerFrame";

export class TypedStatefulServerFrame extends TypedServerFrame {
    private static SizeOfUint32 = 4;

    public currentValueAsBuffer: Buffer;
    public currentValue: number;

    protected usedBytesFromPayload: number = TypedStatefulServerFrame.SizeOfUint32;

    constructor(serverFrame: ServerFrame) {
        super(serverFrame);

        const payload = serverFrame.getPayload_asU8();
        this.currentValueAsBuffer = Buffer.from(payload.buffer, payload.byteOffset, TypedStatefulServerFrame.SizeOfUint32);
        this.currentValue = this.currentValueAsBuffer.readUInt32LE(0);
    }
}