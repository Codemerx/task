import { BigInteger } from 'big-integer';

import { ClientFrame } from "../../proto/StatefulStatelessProtocol_pb";

import { TypedClientResumeFrame } from "./typedClientResumeFrame";
import { stringToBuffer, bigIntegerToString, uint8ToUint8Array } from "../conversionHelpers";
import { ConnectionType } from './connectionType';

export class TypedStatelessClientResumeFrame extends TypedClientResumeFrame {
    constructor(connectionType: ConnectionType, lastSequenceNumber: number, private lastValue: BigInteger) {
        super(connectionType, lastSequenceNumber);
    }

    public toClientFrame() : ClientFrame {
        const result = super.toClientFrame();
        const superPayload = result.getPayload_asU8();

        const lastValueAsUint8Array = stringToBuffer(bigIntegerToString(this.lastValue));
        
        const payload = new Uint8Array(superPayload.length + lastValueAsUint8Array.length);
        payload.set(superPayload, 0);
        payload.set(lastValueAsUint8Array, superPayload.length);
        
        result.setPayloadsize(uint8ToUint8Array(payload.length));
        result.setPayload(payload);

        return result;
    }
}
