import { ClientFrame } from '../../proto/StatefulStatelessProtocol_pb';

import { uint8ToUint8Array, stringToBuffer } from '../conversionHelpers';
import { ClientMessageType } from './clientMessageType';
import { ConnectionType } from './connectionType';

export class TypedClientFrame {
    constructor(private connectionType: ConnectionType, private messageType: ClientMessageType) {
    }

    public toClientFrame() : ClientFrame {
        const clientFrame = new ClientFrame();
        
        clientFrame.setConnectiontype(uint8ToUint8Array(this.connectionType));
        clientFrame.setMessagetype(uint8ToUint8Array(this.messageType));
        clientFrame.setPayloadsize(uint8ToUint8Array(0));
        clientFrame.setPayload(new Uint8Array(0));

        return clientFrame;
    }
}
