import { ClientFrame } from "../../proto/StatefulStatelessProtocol_pb";

import { TypedClientResumeFrame } from "./typedClientResumeFrame";
import { stringToBuffer, uint8ToUint8Array } from "../conversionHelpers";
import { ConnectionType } from "./connectionType";

export class TypedStatefulClientResumeFrame extends TypedClientResumeFrame {
    constructor(connectionType: ConnectionType, private uuid: string, lastSequenceNumber: number) {
        super(connectionType, lastSequenceNumber);
    }

    public toClientFrame() : ClientFrame {
        const result = super.toClientFrame();
        const superPayload = result.getPayload_asU8();

        const uuidAsBuffer = stringToBuffer(this.uuid);
        
        const payload = new Uint8Array(uuidAsBuffer.length + superPayload.length);
        payload.set(superPayload, 0);
        payload.set(uuidAsBuffer, superPayload.length);
        
        result.setPayloadsize(uint8ToUint8Array(payload.length));
        result.setPayload(payload);

        return result;
    }
}
