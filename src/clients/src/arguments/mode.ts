export enum ClientMode {
    Stateless = 0,
    Stateful = 1
}
