import { ArgumentsParseResult } from "./argumentsParseResult";
import { Logger } from "../logger";
import { StatelessClient } from "../statelessClient";
import { ClientMode } from "./mode";
import { Arguments } from "./arguments";

export function parseArguments(args: string[]) : ArgumentsParseResult {
    
    const usageSample = '\n\nUsage:\nnpm start /stateless ServerAddress:Port NumberOfMessages [/test]\nnpm start /stateful ServerAddress:Port [/test NumberOfMessages]\n\nExample: npm start /stateless localhost:9595 15';
    const parsedArguments = new Arguments();

    if (args.length < 2) {
        return {
            isSuccessful: false,
            errorMessage: 'At least two arguments expected.' + usageSample
        };
    }

    if (args[0] === '/stateless') {
        parsedArguments.mode = ClientMode.Stateless;
    } else if (args[0] === '/stateful') {
        parsedArguments.mode = ClientMode.Stateful;
    } else {
        return {
            isSuccessful: false,
            errorMessage: 'The first argument should be either "/stateless" or "/stateful".'  + usageSample
        };
    }

    let connectAddress = args[1];
    let addressParts : string[] = connectAddress.split(":");
    
    if(addressParts.length != 2)
    return {
        isSuccessful: false,
        errorMessage: 'The second argument should be in format ServerAddress:PortNumber.' + usageSample
    };

    parsedArguments.address = addressParts[0];
    parsedArguments.port = Number(addressParts[1]);
    if (isNaN(parsedArguments.port)) {
        return {
            isSuccessful: false,
            errorMessage: 'The port in server address should be a number.' + usageSample
        };
    }

    if (parsedArguments.port <= 1024 || parsedArguments.port > 65535) {
        return {
            isSuccessful: false,
            errorMessage: 'Please, choose a port number between 1024 and 65535.' + usageSample
        };
    }

    if (parsedArguments.mode === ClientMode.Stateless) {
        parsedArguments.numberOfMessages = Number(args[2]);
        if (isNaN(parsedArguments.numberOfMessages)) {
            return {
                isSuccessful: false,
                errorMessage: 'The first argument should be a number.'  + usageSample
            };
        }
    
        if (parsedArguments.numberOfMessages <= 0 || parsedArguments.numberOfMessages > 0xFFFF) {
            return {
                isSuccessful: false,
                errorMessage: 'The last argument should be between 1 and 0xFFFF, inclusively.' + usageSample
            };
        }
    }

    let numberOfArgsWithTestArgument;
    if (parsedArguments.mode === ClientMode.Stateless) {
        numberOfArgsWithTestArgument = 4;
    } else if (parsedArguments.mode === ClientMode.Stateful) {
        numberOfArgsWithTestArgument = 3;
    }

    if (args.length >= numberOfArgsWithTestArgument && args[numberOfArgsWithTestArgument - 1] === '/test') {
        parsedArguments.isTestMode = true;
    }

    if (parsedArguments.mode === ClientMode.Stateful &&
        parsedArguments.isTestMode &&
        args.length === 4) {
        parsedArguments.numberOfMessages = Number(args[3]);
        if (isNaN(parsedArguments.numberOfMessages)) {
            return {
                isSuccessful: false,
                errorMessage: 'The last argument should be a number.'  + usageSample
            };
        }
    
        if (parsedArguments.numberOfMessages <= 0 || parsedArguments.numberOfMessages > 0xFFFF) {
            return {
                isSuccessful: false,
                errorMessage: 'The first argument should be between 1 and 0xFFFF, inclusively.' + usageSample
            };
        }
    }

    return {
        isSuccessful: true,
        parsedArguments
    }
}