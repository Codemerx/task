import { ClientMode } from "./mode";

export class Arguments {
    public mode: ClientMode;
    public numberOfMessages?: number;
    public port: number;
    public address: string;
    public isTestMode: boolean;
}
