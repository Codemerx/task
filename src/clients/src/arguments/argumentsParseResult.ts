import { Arguments } from "./arguments";

export interface ArgumentsParseResult {
    isSuccessful: boolean;
    errorMessage?: string;
    parsedArguments?: Arguments;
}
