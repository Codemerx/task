import { ClientDuplexStream } from 'grpc';
import bigInt = require('big-integer');

import { ServerFrame, ClientFrame } from '../proto/StatefulStatelessProtocol_pb';

import { TypedClientFrame } from './frames/typedClientFrame';
import { ClientMessageType } from './frames/clientMessageType';
import { bigIntegerToString } from './conversionHelpers';
import { Logger } from './logger';
import { TypedStatelessServerFrame } from './frames/typedStatelessServerFrame';
import { TypedStatelessClientResumeFrame } from './frames/typedStatelessClientResumeFrame';
import { ConnectionType } from './frames/connectionType';
import { ClientBase } from './clientBase';

export class StatelessClient extends ClientBase {
    private sum: bigInt.BigInteger = bigInt(0);
    private lastServerFrame: TypedStatelessServerFrame;

    constructor(url: string, numberOfMessagesToReceive: number, isTestMode: boolean) {
        super(url, numberOfMessagesToReceive, isTestMode);

        Logger.log('Creating stateless client with:');
        Logger.log(`\tNumber of values to receive: ${numberOfMessagesToReceive}`);
    }

    protected getClientStartFrame() : ClientFrame {
        const clientStartFrame = new TypedClientFrame(ConnectionType.Stateless, ClientMessageType.Start);
        return clientStartFrame.toClientFrame();
    }

    protected getClientResumeFrame() : ClientFrame {
        const clientResumeFrame = new TypedStatelessClientResumeFrame(ConnectionType.Stateless, this.lastServerFrame.messageSequenceNumber, this.lastServerFrame.currentValue);
        return clientResumeFrame.toClientFrame();
    }

    protected onData(data: ServerFrame, stream: ClientDuplexStream<ClientFrame, ServerFrame>) : void {
        const serverFrame = new TypedStatelessServerFrame(data);
        if (serverFrame.messageSequenceNumber > this.numberOfMessagesToReceive) {
            if (stream.writable) {
                Logger.log('Re-sending close request...');
                this.sendCloseFrame(stream);
                stream.end();
            }

            // At this point we can only ignore the server frame
            return;
        }

        Logger.log(`Value #${serverFrame.messageSequenceNumber} - ${bigIntegerToString(serverFrame.currentValue)}.`)
        this.sum = this.sum.add(serverFrame.currentValue);

        if (serverFrame.messageSequenceNumber === this.numberOfMessagesToReceive) {
            Logger.log('Sending close request...');
            this.sendCloseFrame(stream);
            stream.end();
        }

        this.lastServerFrame = serverFrame;
    }

    protected getResult() : string {
        if (this.isTestMode) {
            return bigIntegerToString(this.sum);
        } else {
            return `Sum: ${bigIntegerToString(this.sum)}`;
        }
    }

    private sendCloseFrame(stream: ClientDuplexStream<ClientFrame, ServerFrame>) : void {
        const cancelClientFrame = new TypedClientFrame(ConnectionType.Stateless, ClientMessageType.Close);
        stream.write(cancelClientFrame.toClientFrame());
    }
}
