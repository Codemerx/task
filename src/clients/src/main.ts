import { parseArguments } from './arguments/parseArguments';
import { StatelessClient } from './statelessClient';
import { Logger } from './logger';
import { ClientMode } from './arguments/mode';
import { StatefulClient } from './statefulClient';

function main(args: string[]) {
    const parseResult = parseArguments(args);
    if (!parseResult.isSuccessful) {
        Logger.log(`Error: ${parseResult.errorMessage}`);

        return;
    }

    Logger.enabled = !parseResult.parsedArguments.isTestMode;

    const port = parseResult.parsedArguments.port;
    const address = parseResult.parsedArguments.address;
    if (parseResult.parsedArguments.mode === ClientMode.Stateless) {
        const client = new StatelessClient(`${address}:${port}`, parseResult.parsedArguments.numberOfMessages, parseResult.parsedArguments.isTestMode);
        client.start(result => {
            Logger.forceLog(result);
        });
    } else {
        const client = new StatefulClient(`${address}:${port}`, parseResult.parsedArguments.isTestMode, parseResult.parsedArguments.numberOfMessages);
        client.start(result => {
            Logger.forceLog(result);
        });
    }
}

main(process.argv.splice(2));   // The first 2 arguments are the name of the program ("node") and the entry .js file ("main.js")