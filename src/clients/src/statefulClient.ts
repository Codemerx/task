import { ClientDuplexStream } from 'grpc';
import v4 = require('uuid/v4');
import { createHash, Hash } from 'crypto';

import { ServerFrame, ClientFrame } from '../proto/StatefulStatelessProtocol_pb';

import { ClientMessageType } from './frames/clientMessageType';
import { Logger } from './logger';
import { TypedStatefulClientFrame } from './frames/typedStatefulClientFrame';
import { TypedStatefulServerFrame } from './frames/typedStatefulServerFrame';
import { ServerMessageType } from './frames/serverMessageType';
import { TypedStatefulServerCloseFrame } from './frames/typedStatefulServerCloseFrame';
import { TypedStatefulClientResumeFrame } from './frames/typedStatefulClientResumeFrame';
import { ConnectionType } from './frames/connectionType';
import { ClientBase } from './clientBase';

export class StatefulClient extends ClientBase {
    private uuid: string;
    private hash: Hash;
    private lastServerFrame: TypedStatefulServerFrame;
    private calculatedHash: string;
    private serverSentHash: string;

    constructor(url: string, isTestMode: boolean, testNumberOfMessages: number) {
        super(url, StatefulClient.getNumberOfMessagesToReceive(isTestMode, testNumberOfMessages), isTestMode);

        this.uuid = v4();
        this.hash = createHash('sha256');

        Logger.log('Creating stateful client with:');
        Logger.log(`\tUUID: ${this.uuid}`);
        Logger.log(`\tNumber of values to receive: ${this.numberOfMessagesToReceive}`);
    }

    private static getNumberOfMessagesToReceive(isTestMode: boolean, testNumberOfMessages: number): number {
        if (isTestMode) {
            return testNumberOfMessages;
        } else {
            return StatefulClient.randomInteger(1, 0xFFFF + 1);
        }
    }
    
    // Generates random integer number in the range [min, max)
    private static randomInteger(min: number, max: number) : number {
        return min + Math.floor(Math.random() * (max - min));
    }

    protected getClientStartFrame() : ClientFrame {
        const clientStartFrame = new TypedStatefulClientFrame(ConnectionType.Stateful, ClientMessageType.Start, this.uuid, this.numberOfMessagesToReceive);
        return clientStartFrame.toClientFrame();
    }

    protected getClientResumeFrame() : ClientFrame {
        const clientResumeFrame = new TypedStatefulClientResumeFrame(ConnectionType.Stateful, this.uuid, this.lastServerFrame.messageSequenceNumber);
        return clientResumeFrame.toClientFrame();
    }

    protected onData(data: ServerFrame, stream: ClientDuplexStream<ClientFrame, ServerFrame>) : void {
        const serverFrame = new TypedStatefulServerFrame(data);
        if (serverFrame.messageSequenceNumber > this.numberOfMessagesToReceive) {
            throw new Error('The message sequence number provided by the server should not be bigger that the total number of message that should be received');            
        }

        Logger.log(`Value #${serverFrame.messageSequenceNumber} - ${serverFrame.currentValue}.`);
        
        this.hash.write(serverFrame.currentValueAsBuffer);

        this.lastServerFrame = serverFrame;

        if (serverFrame.messageType === ServerMessageType.Close) {
            stream.end();

            const closeFrame = new TypedStatefulServerCloseFrame(data);

            this.calculatedHash = this.hash.digest('hex');
            this.serverSentHash = closeFrame.hash.toString('hex');
        }
    }

    protected getResult() : string {
        let result = '';
        if (this.isTestMode) {
            result = this.calculatedHash;
        } else {
            result = `Server hash: ${this.serverSentHash}\nCalculated hash: ${this.calculatedHash}\nResult: `;
            if (this.calculatedHash === this.serverSentHash) {
                result += 'Checksum check passed.';
            } else {
                result += 'Checksum check failed.';
            }
        }

        return result;
    }
}
