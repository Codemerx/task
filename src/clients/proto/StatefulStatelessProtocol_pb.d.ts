// package: 
// file: StatefulStatelessProtocol.proto

/* tslint:disable */

import * as jspb from "google-protobuf";

export class ClientFrame extends jspb.Message { 
    getConnectiontype(): Uint8Array | string;
    getConnectiontype_asU8(): Uint8Array;
    getConnectiontype_asB64(): string;
    setConnectiontype(value: Uint8Array | string): void;

    getMessagetype(): Uint8Array | string;
    getMessagetype_asU8(): Uint8Array;
    getMessagetype_asB64(): string;
    setMessagetype(value: Uint8Array | string): void;

    getPayloadsize(): Uint8Array | string;
    getPayloadsize_asU8(): Uint8Array;
    getPayloadsize_asB64(): string;
    setPayloadsize(value: Uint8Array | string): void;

    getPayload(): Uint8Array | string;
    getPayload_asU8(): Uint8Array;
    getPayload_asB64(): string;
    setPayload(value: Uint8Array | string): void;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ClientFrame.AsObject;
    static toObject(includeInstance: boolean, msg: ClientFrame): ClientFrame.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ClientFrame, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ClientFrame;
    static deserializeBinaryFromReader(message: ClientFrame, reader: jspb.BinaryReader): ClientFrame;
}

export namespace ClientFrame {
    export type AsObject = {
        connectiontype: Uint8Array | string,
        messagetype: Uint8Array | string,
        payloadsize: Uint8Array | string,
        payload: Uint8Array | string,
    }
}

export class ServerFrame extends jspb.Message { 
    getMessagetype(): Uint8Array | string;
    getMessagetype_asU8(): Uint8Array;
    getMessagetype_asB64(): string;
    setMessagetype(value: Uint8Array | string): void;

    getMessagesequencenumber(): Uint8Array | string;
    getMessagesequencenumber_asU8(): Uint8Array;
    getMessagesequencenumber_asB64(): string;
    setMessagesequencenumber(value: Uint8Array | string): void;

    getPayloadsize(): Uint8Array | string;
    getPayloadsize_asU8(): Uint8Array;
    getPayloadsize_asB64(): string;
    setPayloadsize(value: Uint8Array | string): void;

    getPayload(): Uint8Array | string;
    getPayload_asU8(): Uint8Array;
    getPayload_asB64(): string;
    setPayload(value: Uint8Array | string): void;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ServerFrame.AsObject;
    static toObject(includeInstance: boolean, msg: ServerFrame): ServerFrame.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ServerFrame, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ServerFrame;
    static deserializeBinaryFromReader(message: ServerFrame, reader: jspb.BinaryReader): ServerFrame;
}

export namespace ServerFrame {
    export type AsObject = {
        messagetype: Uint8Array | string,
        messagesequencenumber: Uint8Array | string,
        payloadsize: Uint8Array | string,
        payload: Uint8Array | string,
    }
}
