// package: 
// file: StatefulStatelessProtocol.proto

/* tslint:disable */

import * as grpc from "grpc";
import * as StatefulStatelessProtocol_pb from "./StatefulStatelessProtocol_pb";

interface IStatefulStatelessProtocolService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    connect: IStatefulStatelessProtocolService_IConnect;
}

interface IStatefulStatelessProtocolService_IConnect extends grpc.MethodDefinition<StatefulStatelessProtocol_pb.ClientFrame, StatefulStatelessProtocol_pb.ServerFrame> {
    path: string; // "/.StatefulStatelessProtocol/Connect"
    requestStream: boolean; // true
    responseStream: boolean; // true
    requestSerialize: grpc.serialize<StatefulStatelessProtocol_pb.ClientFrame>;
    requestDeserialize: grpc.deserialize<StatefulStatelessProtocol_pb.ClientFrame>;
    responseSerialize: grpc.serialize<StatefulStatelessProtocol_pb.ServerFrame>;
    responseDeserialize: grpc.deserialize<StatefulStatelessProtocol_pb.ServerFrame>;
}

export const StatefulStatelessProtocolService: IStatefulStatelessProtocolService;

export interface IStatefulStatelessProtocolServer {
    connect: grpc.handleBidiStreamingCall<StatefulStatelessProtocol_pb.ClientFrame, StatefulStatelessProtocol_pb.ServerFrame>;
}

export interface IStatefulStatelessProtocolClient {
    connect(): grpc.ClientDuplexStream<StatefulStatelessProtocol_pb.ClientFrame, StatefulStatelessProtocol_pb.ServerFrame>;
    connect(options: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<StatefulStatelessProtocol_pb.ClientFrame, StatefulStatelessProtocol_pb.ServerFrame>;
    connect(metadata: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<StatefulStatelessProtocol_pb.ClientFrame, StatefulStatelessProtocol_pb.ServerFrame>;
}

export class StatefulStatelessProtocolClient extends grpc.Client implements IStatefulStatelessProtocolClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public connect(options?: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<StatefulStatelessProtocol_pb.ClientFrame, StatefulStatelessProtocol_pb.ServerFrame>;
    public connect(metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<StatefulStatelessProtocol_pb.ClientFrame, StatefulStatelessProtocol_pb.ServerFrame>;
}
