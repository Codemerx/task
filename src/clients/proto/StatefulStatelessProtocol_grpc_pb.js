// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var StatefulStatelessProtocol_pb = require('./StatefulStatelessProtocol_pb.js');

function serialize_ClientFrame(arg) {
  if (!(arg instanceof StatefulStatelessProtocol_pb.ClientFrame)) {
    throw new Error('Expected argument of type ClientFrame');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ClientFrame(buffer_arg) {
  return StatefulStatelessProtocol_pb.ClientFrame.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ServerFrame(arg) {
  if (!(arg instanceof StatefulStatelessProtocol_pb.ServerFrame)) {
    throw new Error('Expected argument of type ServerFrame');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ServerFrame(buffer_arg) {
  return StatefulStatelessProtocol_pb.ServerFrame.deserializeBinary(new Uint8Array(buffer_arg));
}


var StatefulStatelessProtocolService = exports.StatefulStatelessProtocolService = {
  connect: {
    path: '/StatefulStatelessProtocol/Connect',
    requestStream: true,
    responseStream: true,
    requestType: StatefulStatelessProtocol_pb.ClientFrame,
    responseType: StatefulStatelessProtocol_pb.ServerFrame,
    requestSerialize: serialize_ClientFrame,
    requestDeserialize: deserialize_ClientFrame,
    responseSerialize: serialize_ServerFrame,
    responseDeserialize: deserialize_ServerFrame,
  },
};

exports.StatefulStatelessProtocolClient = grpc.makeGenericClientConstructor(StatefulStatelessProtocolService);
