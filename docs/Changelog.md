## Changelog

### New
- Implemented stateful server and client
- Added stateful connection test scenario
- Added mixed stateful and stateless connections to the same server test scenario

### Breaking changes
- Protocol
    - Added `MessageType` property in the `ServerFrame`
    - `GUID` changed to `ConnectionType` in the `ClientFrame`
- Client/Server
    - Added new client command-line parameter controlling the mode of the client
    - `number-of-messages` command-line client argument moved to last position


### Improvements
- Fine tuned GRPC built-in back off strategy to replace custom one
- Improved logging