## How to Run

### Prerequisites
- .NET Core SDK (2.1.500 and later)
- Node.js (10.14.1 and later)

### Steps to run the server
1. Open CMD or Terminal
2. cd (project-base-dir)\src\server\Server
3. dotnet run -c Release -- port

Example: dotnet run -c Release -- 9000

### Steps to run the client in stateless mode
1. Open CMD or Terminal
2. cd (project-base-dir)\src\clients
3. npm install
4. npm start /stateless server-address:port number-of-messages

Example: npm start /stateless localhost:9000 20

### Steps to run the client in stateful mode
1. Open CMD or Terminal
2. cd (project-base-dir)\src\clients
3. npm install
4. npm start /stateful server-address:port

Example: npm start /stateful localhost:9000