## How to Test

### Prerequisites
- .NET Core SDK (2.1.500 and later)
- Node.js (10.14.1 and later)

### Modes
The test application have 3 working modes - stateless, stateful and mixed.
- In stateless mode it tests both the server and the client in stateless mode
- In stateful mode it tests both the server and the client in stateful mode
- In mixed mode it tests both the server and the client in both stateless and stateful modes simultaneously

### Steps to run tests
1. Build the server (refer to BuildInstructions.md)
2. Build the client (refer to BuildInstructions.md)
3. Open CMD or Terminal
4. cd (project-base-dir)\src\tests\Tests
5. dotnet run -c Release -- (/stateless|/stateful|/mixed) full-path-to-.NETCore-build-output-server.dll-executable full-path-to-node-build-output-main.js-file number-of-clients-to-start:_optional_

Example: dotnet run -c Release -- /mixed "C:\Task\src\server\Server\bin\Release\netcoreapp2.1\Server.dll" "C:\Task\src\clients\out\main.js" 10