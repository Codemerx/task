## How to Build

### Prerequisites
- .NET Core SDK (2.1.500 and later)
- Node.js (10.14.1 and later)

### Steps to build the server
1. Open CMD or Terminal
2. cd (project-base-dir)\src\server\Server
3. dotnet build -c Release

### Steps to build the client
1. Open CMD or Terminal
2. cd (project-base-dir)\src\clients
3. npm install
4. npm run build